import re
import sys
from typing import cast, Optional, Union
import gitlab
import gitlab.v4.objects


class UserManager(gitlab.v4.objects.users.UserManager):
    def reject(self, id: Union[int, str]):
        path = f"/users/{id}/reject"
        server_data = cast(Optional[bool], self.gitlab.http_post(path))
        return server_data

    def approve(self, id: Union[int, str]):
        path = f"/users/{id}/approve"
        server_data = cast(Optional[bool], self.gitlab.http_post(path))
        return server_data


class Gitlab(gitlab.Gitlab):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.users = UserManager(self)

    def users(self):
        return


class Action:
    def __init__(self, connection, user):
        self.connection = connection
        self.user = user

    def __str__(self):
        return type(self).__name__.lower()

    def perform(self):
        return NotImplementedError()

    @classmethod
    def by_name(cls, name, connection, user):
        # FIXME check that name matches a subclass of action
        name = name.capitalize()
        return getattr(sys.modules[__name__], name)(connection, user)


class Skip(Action):
    def perform(self):
        pass


class Delete(Action):
    def perform(self):
        self.connection.users.delete(id=self.user.id)


class Reject(Action):
    def perform(self):
        self.connection.users.reject(id=self.user.id)


class Approve(Action):
    def perform(self):
        self.connection.users.approve(id=self.user.id)


class RegistrationAnalyzer:
    def __init__(self, connection):
        self.connection = connection

    spammer_domains = [
        "gmail.com",
        "yahoo.com",
        "163.com",
    ]

    temp_mailboxes_domains = [
        "gufum.com",
        "pimmel.top",
        "mailinator.com"
    ]

    def process(self, user):
        parts = user.name.split()
        if len(parts) == 2 and parts[0] == parts[1]:
            return Delete(self.connection, user)
        if user.email and self.spammer_email(user.email):
            return Reject(self.connection, user)
        elif user.email and self.temp_mailbox_domain(user.email):
            return Reject(self.connection, user)
        else:
            return Skip(self.connection, user)

    def spammer_email(self, email):
        domains = self.spammer_domains
        matches = [True for d in domains if re.match(".*[0-9]+@" + d, email)]
        return len(matches) > 0

    def temp_mailbox_domain(self, email):
        domains = self.temp_mailboxes_domains
        matches = [True for d in domains if re.match(".*@" + d, email)]
        return len(matches) > 0
